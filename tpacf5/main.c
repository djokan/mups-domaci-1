#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>

#include "args.h"
#include "model.h"

int main( int argc, char **argv )
{
  struct pb_TimerSet timers;
  struct pb_Parameters *params;
  int rf, k, nbins, npd;
  int* npr;
  float *binb, w;
  long long *DD, **RRS, **DRS;
  size_t memsize;
  struct cartesian *data, **random;
  FILE *outfile;

  pb_InitializeTimerSet( &timers );
  params = pb_ReadParameters( &argc, argv );

  options args;
  parse_args( argc, argv, &args );
    
  pb_SwitchToTimer( &timers, pb_TimerID_COMPUTE );
  nbins = (int)floor(bins_per_dec * (log10(max_arcmin) - 
					 log10(min_arcmin)));
  memsize = (nbins+2)*sizeof(long long);

  // memory for bin boundaries
  binb = (float *)malloc((nbins+1)*sizeof(float));
  if (binb == NULL)
    {
      fprintf(stderr, "Unable to allocate memory\n");
      exit(-1);
    }
  for (k = 0; k < nbins+1; k++)
    {
      binb[k] = cos(pow(10, log10(min_arcmin) + 
			k*1.0/bins_per_dec) / 60.0*D2R);
    }
    
  // memory for DD
  DD = (long long*)malloc(memsize);
  if (DD == NULL)
    {
      fprintf(stderr, "Unable to allocate memory\n");
      exit(-1);
    }
  bzero(DD, memsize);
    
  // memory for RR

  RRS = (long long**)malloc(args.random_count*sizeof(long long*));
  for (rf = 0; rf < args.random_count; rf++){
  RRS[rf] = (long long*)malloc(memsize);
  if (RRS[rf] == NULL)
    {
      fprintf(stderr, "Unable to allocate memory\n");
      exit(-1);
    }
  bzero(RRS[rf], memsize);
  }
  // memory for DR
  
  DRS = (long long**)malloc(args.random_count*sizeof(long long*));
  for (rf = 0; rf < args.random_count; rf++){
    DRS[rf] = (long long*)malloc(memsize);
  if (DRS[rf] == NULL)
    {
      fprintf(stderr, "Unable to allocate memory\n");
      exit(-1);
    }
  bzero(DRS[rf], memsize);
  }
  // memory for input data
  data = (struct cartesian*)malloc
    (args.npoints* sizeof(struct cartesian));
  if (data == NULL)
    {
      fprintf(stderr, 
	      "Unable to allocate memory for % data points (#1)\n", 
	      args.npoints);
      return(0);
    }
    
  random = (struct cartesian**)malloc(sizeof(struct cartesian*)*args.random_count);
  for (rf = 0; rf < args.random_count; rf++)
    random[rf]= (struct cartesian*)malloc(args.npoints*sizeof(struct cartesian));
  
  npr = (int*)malloc(sizeof(int*)*args.random_count);
  
  if (random == NULL)
    {
      fprintf(stderr, 
	      "Unable to allocate memory for % data points (#2)\n", 
	      args.npoints);
      return(0);
    }

  printf("Min distance: %f arcmin\n", min_arcmin);
  printf("Max distance: %f arcmin\n", max_arcmin);
  printf("Bins per dec: %i\n", bins_per_dec);
  printf("Total bins  : %i\n", nbins);

  // read data file
  pb_SwitchToTimer( &timers, pb_TimerID_IO );
  npd = readdatafile(params->inpFiles[0], data, args.npoints);
  pb_SwitchToTimer( &timers, pb_TimerID_COMPUTE );
  if (npd != args.npoints)
    {
      fprintf(stderr, 
	      "Error: read %i data points out of %i\n", 
	      npd, args.npoints);
      return(0);
    }

  // compute DD
  doCompute(data, npd, NULL, 0, 1, DD, nbins, binb);

    for (rf = 0; rf < args.random_count; rf++)
    {
      // read random file
      pb_SwitchToTimer( &timers, pb_TimerID_IO );
      npr[rf] = readdatafile(params->inpFiles[rf+1], random[rf], args.npoints);
      pb_SwitchToTimer( &timers, pb_TimerID_COMPUTE );
      
      if (npr[rf] != args.npoints)
        {
	  fprintf(stderr, 
		  "Error: read %i random points out of %i in file %s\n", 
		  npr[rf], args.npoints, params->inpFiles[rf+1]);
	  return(0);
        }
    }
  
    #pragma omp parallel for schedule(dynamic,1)
    for (rf = 0; rf < args.random_count; rf++)
    {  
      // compute RR
      doCompute(random[rf], npr[rf], NULL, 0, 1, RRS[rf], nbins, binb);

      // compute DR

      
    }


    #pragma omp parallel for schedule(dynamic,1)
    for (rf = 0; rf < args.random_count; rf++)
    {  
          doCompute(data, npd, random[rf], npr[rf], 0, DRS[rf], nbins, binb);
          
    }

    long long *RRSI, *DRSI;

    RRSI = malloc(memsize);
    DRSI = malloc(memsize);

    bzero(RRSI, memsize);
    bzero(DRSI, memsize);
    
    for (rf = 0; rf < args.random_count; rf++)
    {
      for (k = 1; k < nbins+1; k++){
      RRSI[k] += RRS[rf][k];
      DRSI[k] += DRS[rf][k];
      }
      free(RRS[rf]);
      free(DRS[rf]);
    }
    free(RRS);
    free(DRS);
  // compute and output results
  if ((outfile = fopen(params->outFile, "w")) == NULL)
    {
      fprintf(stderr, 
	      "Unable to open output file %s for writing, assuming stdout\n", 
	      params->outFile);
      outfile = stdout;
    }

  pb_SwitchToTimer( &timers, pb_TimerID_IO );
  for (k = 1; k < nbins+1; k++)
    {
      fprintf(outfile, "%d\n%d\n%d\n", DD[k], DRSI[k], RRSI[k]);      
    }

  if(outfile != stdout)
    fclose(outfile);

  // free memory
  for (rf = 0; rf < args.random_count; rf++)
    free(random[rf]);
  free(data);
  free(random);
  free(binb);
  free(DD);
  free(RRSI);
  free(DRSI);
  
  pb_SwitchToTimer( &timers, pb_TimerID_NONE );
  pb_PrintTimerSet( &timers );
  pb_FreeParameters( params );
}

